<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Datos Concurso</title>
<link rel="stylesheet" href="css/styleEditarConcurso.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarDatosConcurso.php" method="POST">
		     <p>
	         <fieldset id="datosConcurso">
             <legend>DATOS DEL CONCURSO AL CUAL APLICA</legend>
			  <p>	
	            <label>Facultad:</label>
			     <select name="facultad" onchange="ajustar(this,this.form.unidad);">
				 <option>Seleccione una</option>
				 <option value="agropecuarias">FACULTAD DE CIENCIAS AGROPECUARIAS Y RECURSOS NATURALES</option>
				 <option value="ingenierias">FACULTAD DE CIENCIAS BÁSICAS E INGENIERÍA</option>
				 <option value="economicas">FACULTAD DE CIENCIAS ECONÓMICAS</option>
				 <option value="humanas">FACULTAD DE CIENCIAS HUMANAS Y DE LA EDUCACIÓN</option>
				 <option value="salud">FACULTAD DE CIENCIAS DE LA SALUD</option>
				</select>
		      </p>
		      <p>
		         <label>Unidad Académica: <br>(Escuela, Departamento o Instituto)</label>
		         <select name="unidad" onchange="ajustar(this,this.form.area);">
		         <option>Seleccione una</option>
	          </select>
		      </p>
		      <p> 
		         <label>Area:</label>
		         <select name="area" onchange="ajustar(this,this.form.concurso2);">
		         <option>Seleccione una</option>
	             </select>
		      </p>
		      <p>
		       <label>N°.Concurso:</label>
		       <select name="concurso2">
		       <option>Seleccione una</option>
	          </select> 
		      </p>
			  
			   <input type="submit" value="Guardar" id="guardar1">
          </fieldset>
		  </p>
		  
		 
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

