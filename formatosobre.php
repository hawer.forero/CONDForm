<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/styleFormulario.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
<script src="js/validar.js"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
       
	    
		 <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
		</section>
		<section id="titulo">
        <h1>FORMATO PRESENTACIÓN SOBRE <br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form id="form2" action="insertar1.php" method="POST">
		  
		     <p>
	         <fieldset id="datosConcurso">
             <legend>DATOS DEL CONCURSO AL CUAL APLICA</legend>
			  <p>	
	            <label>Facultad:</label>
			     <select name="facultad" onchange="ajustar(this,this.form.unidad);">
				  <option>Seleccione una</option>
				 <option value="agropecuarias">FACULTAD DE CIENCIAS AGROPECUARIAS Y RECURSOS NATURALES</option>
				 <option value="ingenierias">FACULTAD DE CIENCIAS BÁSICAS E INGENIERÍA</option>
				 <option value="economicas">FACULTAD DE CIENCIAS ECONÓMICAS</option>
				 <option value="humanas">FACULTAD DE CIENCIAS HUMANAS Y DE LA EDUCACIÓN</option>
				 <option value="salud">FACULTAD DE CIENCIAS DE LA SALUD</option>
				</select>
		      </p>
		      <p>
		         <label>Unidad Académica: <br>(Escuela, Departamento o Instituto)</label>
		         <select name="unidad" onchange="ajustar(this,this.form.area);">
		          <option>Seleccione una</option>
	          </select>
		      </p>
		      <p> 
		         <label>Area:</label>
		         <select name="area" onchange="ajustar(this,this.form.concurso);">
		         <option>Seleccione una</option>
	             </select>
		      </p>
		      <p>
		       <label>N°.Concurso (Codigo):</label>
		       <select name="concurso">
		       <option>Seleccione nna</option>
	          </select> 
		      </p>
          </fieldset>
		  </p>
		  
		  <p>
		   <fieldset id="datosPersonales">
		    <legend>DATOS PERSONALES</legend>	
			<section id="sobreDER">
			 <p>
			   <label id="tipo">Tipo/N° Identificación:</label>
			   <select name="tipoid">
		       <option>C.C</option>
			   <option>C.E</option>
			   <option>PAS</option>
	           </select>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			    </p>
			
               <p>
               <label>Nombres:</label>
			   <input type='text'  value='' name="nombres"  onChange="javascript:this.value=this.value.toUpperCase();" required>
                </p>			  
			  <p>
			   <label>Apellidos:</label>
			   <input type='text' value='' name="apellidos"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		       
			   </p>
		        <p>
		        <label>Sexo:</label>
			    <input type="radio" name="sexo" value='FEMENINO' id="sexo"><label id="f">F</label>
			    <input type="radio" name="sexo" value='MASCULINO' id="sexo"><label id="m">M</label>
				</p>
				<p>
				<label>Teléfono:</label>
			    <input type='text'  value='' name="telefono">
		        </p>
		        <p>
			    
			    <label id="cel">Celular:</label>
			    <input type='text'  value='' name="celular" id="celular1" required>
				</p>
		        <p class="correo">
			    <label>Email:</label>
			    <input type='email' value='' name="email" id="email" required>
		       </p>
			   </section>
			   <section id ="sobreIZQ">
			   <p>
			               <label>Departamento:</label>
			               <input type='text'  value='' name="depCorrespondencia"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		                 </p>
                         <p>		
			                <label>Municipio:</label>
			                <input type='text' value='' name="munCorrespondencia"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		                 </p>
		                 <p>
			                <label>Dirección: </label>
			                <input type='text'  value='' name="direccion" id="direccion" onChange="javascript:this.value=this.value.toUpperCase();" required> 
		                    
		                 </p>
						 <p>
			                <label>Barrio: </label>
			                <input type='text'  value='' name="barrio" id="direccion" onChange="javascript:this.value=this.value.toUpperCase();" required> 
		                    
		                 </p>
				</section>
            </fieldset>
		    </p>
			 
		  <input type="submit" value="IMPRIMIR" id="guardar1" onClick="return validaciones()">
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html>