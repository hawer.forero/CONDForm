<!doctype html>
<?php
require('seguridad.php');
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
$us = $_SESSION['usuario'];
$idExperienciaDocente = $_GET["id"];
conectar_base_datos();
 
  $query = mysql_query("SELECT institucion,DATE_FORMAT(fechaVinculacion,'%e'),DATE_FORMAT(fechaVinculacion,'%m'),DATE_FORMAT(fechaVinculacion,'%Y'),DATE_FORMAT(fechaFinalizacion,'%e'),DATE_FORMAT(fechaFinalizacion,'%m'),DATE_FORMAT(fechaFinalizacion,'%Y'),folio,horasSemana,semanasSemestre,horasSemestre FROM `cd_experienciadocente` WHERE `identificacion`='$us'AND `idExperienciaDocente`=$idExperienciaDocente");
  $row = mysql_fetch_array($query);
  if($row[2]==01){$mes='Enero';}
  if($row[2]==02){$mes='Febrero';}
  if($row[2]==03){$mes='Marzo';}
  if($row[2]==04){$mes='Abril';}
  if($row[2]==05){$mes='Mayo';}
  if($row[2]==06){$mes='Junio';}
  if($row[2]==07){$mes='Julio';}
  if($row[2]==8){$mes='Agosto';}
  if($row[2]==9){$mes='Septiembre';}
  if($row[2]==10){$mes='Octubre';}
  if($row[2]==11){$mes='Noviembre';}
  if($row[2]==12){$mes='Diciembre';}
   if($row[5]==01){$mes2='Enero';}
  if($row[5]==02){$mes2='Febrero';}
  if($row[5]==03){$mes2='Marzo';}
  if($row[5]==04){$mes2='Abril';}
  if($row[5]==05){$mes2='Mayo';}
  if($row[5]==06){$mes2='Junio';}
  if($row[5]==07){$mes2='Julio';}
  if($row[5]==8){$mes2='Agosto';}
  if($row[5]==9){$mes2='Septiembre';}
  if($row[5]==10){$mes2='Octubre';}
  if($row[5]==11){$mes2='Noviembre';}
  if($row[5]==12){$mes2='Diciembre';}
  

?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleInsertarExperienciaD.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
       <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="editarExperienciaMC.php" method="POST">
		    <input name="idExperienciaDocente" type="hidden" value="<?php echo"$idExperienciaDocente";?>" > 
		     <p>
			 <fieldset id="experienciaDocente">
                   <legend>EXPERIENCIA DOCENTE</legend>
				   <p>
				  <fieldset id="catedra">
			       <legend id="experienciat">MODALIDAD CÁTEDRA</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
					 <p>
						 <label>Institución:</label>
						 <input type='text' value='<?php echo"$row[0]";?>' name="institucion" onChange="javascript:this.value=this.value.toUpperCase();" id="large" required>
				    </p>
					<p>
						 <label>N°horas/Semana:</label>
						 <input type='number' value='<?php echo"$row[8]";?>' name="horasSemana" step="0.1" min="0" max="1000" required>
				    </p>
					<p>
						 <label>N°Semanas/Semestre:</label>
						 <input type='number' value='<?php echo"$row[9]";?>' name="semanasSemestre" step="0.1" min="0" max="1000" required>
				    </p>
					<p>
						 <label>Total horas Semestre:</label>
						 <input type='number' value='<?php echo"$row[10]";?>' name="horasSemestre" step="0.1" min="0" max="99999" required>
				    </p>
					<p>
			                <label id="fecha">Fecha Vinculación:</label>
			                 <select name="dia">
							 <option><?php echo"$row[1]";?></option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes" id="mes">
							  <option value='<?php echo"$row[2]";?>'><?php echo"$mes";?></option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano">
							 <option> <?php echo"$row[3]";?></option>
							 <?php  for($i=1924;$i<=2014;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						 <p>
			                <label id="fecha">Fecha Finalización:</label>
			                 <select name="dia2">
							 <option><?php echo"$row[4]";?></option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes2" id="mes">
							  <option value='<?php echo"$row[5]";?>'><?php echo"$mes2";?></option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano2">
							   <option><?php echo"$row[6]";?></option>
							 <?php  for($i=1924;$i<=2014;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						  <p>
						 <label>Folio:</label>
						 <input type='number' value='<?php echo"$row[7]";?>' name="folio" min="1" max="1000" required>
				    </p>
					 <p>
						 <input type="submit" value="Guardar" id="nav">
						 </p>
	     </fieldset>
               </p>
						
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

