<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleInsertarExperienciaD.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarExperienciaP.php" method="POST">
		      <p>
			   <input type="hidden" value="profesional" name="tipo">
			  </p>
		     <p>
			 <fieldset id="experienciaProfesional">
                   <legend>EXPERIENCIA PROFESIONAL</legend>
				   
				  
					 <p>
						 <label>Tipo de Certificación:</label>
						 <select name="tipoVinculacion">
						 <option></option>
						 <option value="C.LAB">C.LAB - CERTIFICACION LABORAL</option>
						 <option value="D.JUR">D.JUR - DECLARACION JURAMENTADA</option>
						 <option value="C.CPS">C.CPS - CERTIFICADO DE CUMPLIMIENTO DE CONTRATO DE PRESTACIÓN DE SERVICIOS</option>
						 </select>
				    </p>
					<p>
						 <label>Empresa o Institución:</label>
						 <input type='text' value='' name="empresa" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
						 <label>Cargo o Función:</label>
						 <input type='text' value='' name="cargo" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
			                <label id="fecha">Fecha Vinculación:</label>
							  <select name="ano">
							   <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
							 <select name="mes" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="dia">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 
			               
		                 </p>
						 <p>
			                <label id="fecha">Fecha Finalización:</label>
							  <select name="ano2">
							   <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			               
			                 <select name="mes2" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="dia2">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						  <p>
						 <label>Folio:</label>
						 <input type='number' value='' name="folio" min="1" max="1000"  required>
				    </p>
					 <p>
						 <input type="submit" value="Guardar" id="guardar1">
						 </p>
	     
						
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

