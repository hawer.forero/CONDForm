<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleInsertarExperienciaD.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
      <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarExperienciaTC.php" method="POST">
		     <p>
			 <fieldset id="experienciaDocente">
                   <legend>EXPERIENCIA DOCENTE</legend>
				   <p>
				  <fieldset>
			       <legend id="experienciat">TIEMPO COMPLETO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
			         <input name="dedicacion" type="hidden" value="TC" > 
					 <p>
						 <label>Institución:</label>
						 <input type='text' value='' name="institucion" id="large" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
			                <label id="fecha">Fecha Vinculación:</label>
			                 <select name="dia">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano">
							   <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						 <p>
			                <label id="fecha">Fecha Finalización:</label>
			                 <select name="dia2">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes2" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano2">
							   <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						  <p>
						 <label>Folio:</label>
						 <input type='number' value='' name="folio" min="1" max="1000" required>
				    </p>
					 <p>
						 <input type="submit" value="Guardar" id="nav">
						 </p>
	     </fieldset>
               </p>
						
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

