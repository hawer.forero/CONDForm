<?php


session_start(); 


if ($_SESSION["autentificado"] != "SI") { 
   	
   	header("Location: index.php"); 
   	
   	exit(); 
}	




?>
<html>
<head>
<meta charset="utf-8"/>
<title>ConD</title>
<link rel="stylesheet" href="css/styleFormulario.css"/>
<script src="js/agregar-fila.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo '<a href="logout.php">Salir</a>';?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>SISTEMA DE CONSULTA<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		<nav>
		   <ul>
		      <li><a href="menu.php">Inicio</a></li>
			  <li><a href="inscripcion.php">Inscritos</a></li>
		   </ul>
		</nav>
		<fieldset>
		<legend>EXPORTAR EXCEL</legend>
		<p>
		<label>1. Consolidado inscritos a la fecha</label>
		<a href="consolidado.php"><input class="boton" type="button" value="Descargar"> </a>
		</p>
		<p>
		 <form id="form2" action="descargarExcel.php" method="POST">
	          <label> 2. Información por aspirante para evaluación </label> 
			 <p>
			   <label id="tipo">N° Identificación:</label>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			    <input type="submit" value="Descargar" id="guardar1">
			 </p>
		 
	     </form>
		</p>
		
		
		</fieldset>
		<p>
		<fieldset>
		<legend>GENERAR CÓDIGO RECUPERACIÓN DE CONTRASEÑA</legend>
		<p>
		 <form id="form2" action="generarCodigo.php" method="POST">
		<label>  </label> 
			 <p>
			   <label id="tipo">N° Identificación del Aspirante:</label>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			    <input type="submit" value="Generar Codigo" id="generar">
			 </p>
		</form>
		</p>
		</fieldset>	
	    </p>
		<p>
		<fieldset>
		<legend>CONSULTAR</legend>
		<p>
		<fieldset id="hoja">
		<legend id="titulo2">FORMATO HOJA DE VIDA</legend>
		<p>
		 <form id="form2" action="descargarformatohv.php" method="POST">
		<label>  </label> 
			 <p>
			   <label>N° Identificación:</label>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			   </p>
			   <p>
			    <input type="submit" value="Buscar" id="generar">
			 </p>
		</form>
		</p>
		</fieldset>	
	    </p>
		<p>
		<fieldset id="sobre">
		<legend id="titulo2">FORMATO PRESENTACIÓN SOBRE</legend>
		<p>
		 <form id="form2" action="descargarformatosobre.php" method="POST">
		<label>  </label> 
			 <p>
			   <label>N° Identificación:</label>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			   </p>
			   <p>
			    <input type="submit" value="Buscar" id="generar">
			 </p>
		</form>
		</p>
		</fieldset>	
	    </p>
		</fieldset>	
	    </p>
		<p>
		<fieldset>
		<legend>HOJAS DE VIDA RADICADAS</legend>
		<p>
		<label>1. Acta de cierre recepción hojas de vida </label>
		<a href="acta.php"><input class="boton" type="button" value="Descargar"> </a>
		</p>
		</fieldset>
		</p>
	 
	</article>




</body>
</html> 
 
 