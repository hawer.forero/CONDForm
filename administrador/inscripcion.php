<?php 
session_start(); 


if ($_SESSION["autentificado"] != "SI") { 
   	
   	header("Location: index.php"); 
   	
   	exit(); 
}	
include("conexion.php")





?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COND</title>
<link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
<link rel="stylesheet" href="css/styleFormulario.css"/>
<style type="text/css">
	/* CSS demo */
	#content{
		padding:20px 0 0 10px
	}
	#content .filtro{
		overflow:hidden;
		padding-bottom:15px
	}
	#content .filtro select{
		width:100px
	}
	#content .filtro ul{
		list-style:none;
		padding:0
	}
	#content .filtro li{
		float:left;
		display:block;
		margin:0 5px
	}
	#content .filtro li a{
		color:#006;
		position:relative;
		top:5px;
		text-decoration:underline
	}
	#content .filtro li label{
		float:left;
		padding:4px 5px 0 0
	}
	#content table{
		border-collapse:collapse;
		width:940px;
	}
	#content table th{
		border:1px solid #999;
		padding:8px;
		background:#F8F8F8
	}
	#content table th span{
		cursor:pointer;
		padding-right:12px
	}
	#content table th span.asc{
		background:url(assets/imgs/sorta.gif) no-repeat right center;
	}
	#content table th span.desc{
		background:url(assets/imgs/sortd.gif) no-repeat right center;
	}
	#content table td{
		border:1px solid #999;
		padding:6px
	}
	
</style>
<link rel="stylesheet" type="text/css" href="assets/jqueryui/css/smoothness/jquery-ui-1.8.16.custom.css"/>

<script type="text/javascript" src="assets/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="assets/jqueryui/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>

</head>
<body>
<article id="contenido1">
<section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo '<a href="logout.php">Salir</a>';?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>SISTEMA DE CONSULTA<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		<nav>
		   <ul>
		      <li><a href="menu.php">Inicio</a></li>
			  <li><a href="inscripcion.php">Inscritos</a></li>
		   </ul>
		</nav>
		<p>
		<fieldset>
		<legend>TABLA INSCIPCIONES</legend>
	<div id="content">
        <div class="filtro">
        	<form id="frm_filtro" method="post" action="">
                <ul>
				    <li><label>Facultad:</label>
                        <select name="facultad">
                            <option value="0">--</option>
                            <!-- Listar Paises -->
                            <?php
                            $query = mysql_query("SELECT * FROM cd_facultad f ORDER BY f.nombre ASC"); 
                            while($row = mysql_fetch_array($query)){
                                ?>
                                <option value="<?php echo $row['idFacultad'] ?>">
                                    <?php echo $row['nombre'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>                	
                    </li>
					<li><label>Unidad Academica:</label>
                        <select name="unidad">
                            <option value="0">--</option>
                            <!-- Listar Paises -->
                            <?php
                            $query = mysql_query("SELECT * FROM cd_unidadacademica u ORDER BY u.nombre ASC"); 
                            while($row = mysql_fetch_array($query)){
                                ?>
                                <option value="<?php echo $row['idUnidadAcademica'] ?>">
                                    <?php echo $row['nombre'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>                	
                    </li>
					<li><label>Area:</label>
                        <select name="area">
                            <option value="0">--</option>
                            <!-- Listar Paises -->
                            <?php
                            $query = mysql_query("SELECT * FROM cd_concurso c ORDER BY c.area ASC"); 
                            while($row = mysql_fetch_array($query)){
                                ?>
                                <option value="<?php echo $row['idConcurso'] ?>">
                                    <?php echo $row['area'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>                	
                    </li>
						<li><label>Concurso:</label>
                        <select name="concurso">
                            <option value="0">--</option>
                            <!-- Listar Paises -->
                            <?php
                            $query = mysql_query("SELECT * FROM cd_concurso c ORDER BY c.idUnidadAcademica ASC"); 
                            while($row = mysql_fetch_array($query)){
                                ?>
                                <option value="<?php echo $row['idConcurso'] ?>">
                                    <?php echo $row['idConcurso'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>                	
                    </li>
					<li>
                        <br>
                        
					<p>
                   <li> <label>Identificación:</label> <input type="text" name="identificacion"/></li>
                    <li><label>Nombre:</label> <input type="text" name="nombre"/></li>
                     <li> <label>Apellido:</label> <input type="text" name="apellido"/></li>
                    </p>

					</li>
                    <br>
					<li>
                   <p>
                    	<button type="button" id="btnfiltrar">Filtrar</button>
                       </p>
                        </li>
                 <li>						
                  <p>
                    	<a href="javascript:;" id="btncancel">Todos</a>
                 </p>
				 <li>
                </ul>
            </form>
        </div>
        <table cellpadding="0" cellspacing="0" id="data">
        	<thead>
            	<tr>
				    <th width="12%"><span title="facultad">FACULTAD</span></th>
					<th width="12%"><span title="unidad">UNIDAD ACADEMICA</span></th>
					<th width="12%"><span title="area">AREA</span></th>
                    <th width="12%"><span title="concurso">CONCURSO</span></th>
                    <th width="12%"><span title="identificacion">IDENTIFICACION</span></th>
                    <th width="30%"><span title="nombre">NOMBRE</span></th>
					<th width="30%"><span title="nombre">RADICO</span></th>
					 <th><span title="nombre_pais">Accion</span></th>
                </tr>
            </thead>
            <tbody>
            	
            </tbody>
        </table>
	</div> 
	</fieldset>
	</p>
</article>	
</body>
</html>