<?php
header('Content-Type: text/html; charset=UTF-8'); 
session_start(); 


if ($_SESSION["autentificado"] != "SI") { 
    
    header("Location: index.php"); 
    
    exit(); 
} 


require('seguridad.php');
conectar_base_datos();

// CREAR PDF 
include('class.ezpdf.php');
$pdf =& new Cezpdf('a4');
$pdf->selectFont('fonts/Helvetica.afm');
$datacreator = array (
                    'Title'=>'Acta de cierre',
                    'Author'=>'Universidad de los LLanos',
                    'Subject'=>'PDF con Tablas',
                    'Creator'=>'Universidad de los LLanos',
                    'Producer'=>'Universidad de los LLanos'
                    );
$pdf->addInfo($datacreator);
$pdf->ezStartPageNumbers(300,20,12,'','',1);
//$centrar=array('xOrientation'=>'center');

//ENCABEZADO
$text_option=array('justification'=>'center',20);

$pdf->ezText("<b>UNIVERSIDAD DE LOS LLANOS</b>",14,$text_option);
$pdf->ezText("Vicerrectoria Académica",14,$text_option);
$pdf->ezText("\n",12);
$pdf->ezText("<b>ACTA DE CIERRE RECEPCIÓN HOJAS DE VIDA EN EL MARCO </b>",12,$text_option);
$pdf->ezText("<b>DE LA CONVOCATORIA 02-P-2014</b>",12,$text_option);
$pdf->ezText("
En Villavicencio siendo las cinco (5 p.m.). Del día martes once (11) de noviembre de 2014, se da 
por  terminada  la  recepción  de  hojas  de  vida.  La  siguiente  es  la  relación  de  hojas de vida 
recibidas hasta la fecha:
",12);$pdf->ezText("\n",1);



$pdf->ezText("<b>  1.	FACULTAD DE CIENCIAS AGROPECUARIAS Y RECURSOS NATURALES</b>",12);
$pdf->ezText("\n",2);
$pdf->ezText("<b>  1.1 Unidad Académica: Instituto de Acuicultura de los Llanos (IALL) \n</b>",12);
$pdf->ezText("\n",2);

$tabla1 = array( 
'showHeadings'=>1, 
'shaded'=>1, 
'showLines'=>1, 
'colGap'=>2, 
'xOrientation'=>'center', 
'cols'=>array( 
"num" => array('justification'=>'center', 'width' => '60'), 
"nom" => array('justification'=>'center', 'width' => '260')));
$titles = array('num'=>'<b>No</b>','nom'=>'<b>NOMBRES Y APELLIDOS</b>');

$total = 0;

$con='01-IALL-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-IALL-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data2[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data2,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  1.2 Unidad Académica: Escuela de Ingeniería en Ciencias Agrícolas \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data3[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data3,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data4[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data4,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data5[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data5,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-EICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data6[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data6,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='05-EICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data7[] = array('num'=>$i,'nom'=>$nombre);

}
$pdf->ezTable($data7,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  1.3 Escuela Académica: Escuela de Ciencias Animales  \n</b>",12);
$pdf->ezText("\n",2);

$con='01-ECA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data8[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data8,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-ECA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data9[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data9,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-ECA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data10[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data10,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-ECA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data11[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data11,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;



$pdf->ezText("\n",12);
$pdf->ezText("<b>  2. FACULTAD DE CIENCIAS BÁSICAS E INGENIERÍA</b>",12);
$pdf->ezText("\n",2);
$pdf->ezText("<b>  2.1 Unidad Académica: Departamento de Biología y Química \n</b>",12);
$pdf->ezText("\n",2);

$con='01-DBQ-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$dataA[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($dataA,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-DBQ-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data13[] = array('num'=>$i,'nom'=>$nombre);

}
$pdf->ezTable($data13,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-DBQ-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data14[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data14,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  2.2	Unidad Académica: Departamento de Matemáticas y Física  \n</b>",12);
$pdf->ezText("\n",2);

$con='01-DMF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data15[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data15,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-DMF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data16[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data16,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$total.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-DMF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data17[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data17,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  2.3 Unidad Académica: Instituto de Ciencias Ambientales   \n</b>",12);
$pdf->ezText("\n",2);


$con='01-ICA-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data18[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data18,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  2.4 Unidad Académica: Escuela de Ingeniería    \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EI-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data19[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data19,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EI-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data20[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data20,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EI-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data21[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data21,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  3. FACULTAD DE CIENCIAS ECONÓMICAS</b>",12);
$pdf->ezText("\n",2);
$pdf->ezText("<b>  3.1 Unidad Académica: Escuela de Economía y Finanzas   \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EEF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data22[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data22,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EEF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data23[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data23,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EEF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data24[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data24,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-EEF-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data25[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data25,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  3.2 Unidad Académica: Escuela de Administración y Negocios     \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EAN-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data26[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data26,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EAN-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data27[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data27,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EAN-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data28[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data28,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  4. FACULTAD DE CIENCIAS HUMANAS Y DE LA EDUCACIÓN</b>",12);
$pdf->ezText("\n",2);
$pdf->ezText("<b>  4.1 Unidad Académica: Escuela de Pedagogía y Bellas Artes    \n</b>",12);
$pdf->ezText("\n",2);


$con='01-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data29[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data29,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data30[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data30,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data31[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data31,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data32[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data32,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='05-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data33[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data33,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='06-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data34[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data34,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='07-EPB-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data35[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data35,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  4.2 Unidad Académica: Escuela de Humanidades      \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EH-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data36[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data36,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EH-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data37[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data37,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  5. FACULTAD DE CIENCIAS DE LA SALUD</b>",12);
$pdf->ezText("\n",2);
$pdf->ezText("<b>  5.1 Unidad Académica: Escuela de Cuidado     \n</b>",12);
$pdf->ezText("\n",2);

$con='01-EC-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data38[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data38,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-EC-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data39[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data39,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-EC-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data40[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data40,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-EC-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data41[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data41,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='05-EC-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data42[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data42,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;

$pdf->ezText("\n",12);
$pdf->ezText("<b>  5.2 Unidad Académica: Escuela de Salud Pública       \n</b>",12);
$pdf->ezText("\n",2);

$con='01-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data43[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data43,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='02-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data44[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data44,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='03-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data45[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data45,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='04-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data46[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data46,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='05-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data47[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data47,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$con='06-ESP-02-14';
$pdf->ezText("<b>".$con."</b>",12,$text_option);
$pdf->ezText("\n",1);
$i=0;
$row =mostrar($con);
while ($rs = mysql_fetch_array($row)){
$i=$i+1;
$nombre= $rs[0]." ".$rs[1]." ".$rs[2];
$data48[] = array('num'=>$i,'nom'=>$nombre);
}
$pdf->ezTable($data48,$titles,'',$tabla1);
$pdf->ezText("<b>Total: (".$i.")</b>",12,$text_option);
$total = $total + $i;
$pdf->ezText("\n",9);

$pdf->ezText("
En total se recibieron (".$total.") hojas de vida",12);

$pdf->ezText("
En constancia firman:",12);

$pdf->ezText("\n",50);


$pdf->ezText("<b>WILTON ORACIO CALDERÓN CAMACHO                   GIOVANY QUINTERO REYES </b>",12,$text_option);
$pdf->ezText("                          Vicerrecctor Académico                                               Secretario General ",12);
$pdf->ezText("\n",50);


$pdf->ezText("<b>ANDREA DEL PILAR ALVAREZ TORRES</b>",12,$text_option);
$pdf->ezText("Coordinador Concurso Docentes de Planta",12,$text_option);





ob_end_clean();
$pdf->ezStream();
$pdf->ezStopPageNumbers();


 ?>