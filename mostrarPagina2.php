<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  $query = "SELECT idFormacionAcademica,modalidad,tituloObtenido,fechaGrado,nTarjeta  FROM cd_formacionacademica 
  WHERE identificacion = '$us' AND (`modalidad`='UNIVERSITARIA'OR`modalidad`='TECNOLOGICA')";
  $row = mysql_query($query);
  
   $query2 = "SELECT idFormacionAcademica,modalidad,tituloObtenido,fechaGrado,semestresAprobados,graduado  FROM cd_formacionacademica 
  WHERE identificacion = '$us' AND (`modalidad`='ESPECIALIZACIÓN'OR`modalidad`='MAESTRIA'OR`modalidad`='DOCTORADO')";
  $row2 = mysql_query($query2);
  
  $query3 = "SELECT * FROM cd_idioma 
  WHERE identificacion = '$us'";
  $row3 = mysql_query($query3);
  
  $consulta = mysql_query("SELECT idioma FROM cd_aspirante WHERE identificacion = '$us'");
  $row4 = mysql_fetch_array($consulta);
  
  

 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/stylePagina2.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
         <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		
        <fieldset id="formacionAcademica">
                   <legend>ESTUDIOS EDUCACIÓN SUPERIOR</legend>
				   <p>
				   <label id="inf">
				   En este recuadro usted deberá ingresar cronológicamente (del más actual al más antiguo), 
				   los datos de los títulos tecnológicos, de pregrado y posgrado que ha obtenido o que se encuentra 
				   actualmente cursando.
				   </p>
				   <p>
				  <fieldset>
			       <legend id="pregradot">TECNOLÓGICO Y PREGRADO</legend>
				    
				  
				   
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtencion de los titulos.</label>
				  
			        <table id="tablaPregrado" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPregradoTitulos"> 
		   	             <th id="pre1"><label>Modalidad Académica</label></th>
			             <th id="pre2"><label>Nombre   Título   Obtenido</label></th> 
		   	             <th id="pre3"><label>Fecha Grado</label></th> 
			             <th id="pre4"><label>N°Tarjeta Profesional(*)</label></th> 
						 <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php while ($rs = mysql_fetch_assoc($row)) { ?>
                        <tr>
						<td><label><?php echo $rs['modalidad']; ?></label></td>
						<td><label><?php echo $rs['tituloObtenido']; ?></label></td>
						<td><label><?php echo $rs['fechaGrado']; ?><label></td>
						<td><label><?php echo $rs['nTarjeta']; ?><label></td>
						<td><a href="formeditarPregrado.php?id=<?php echo $rs['idFormacionAcademica']; ?>">Editar</a></td>
						<td><a href="borrarPregrado.php?id=<?php echo $rs['idFormacionAcademica']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
					 <label id="inf">* Si la profesión está reglamentada.</label>
					<p>
					<td><a href="forminsertarFormacionPregrado.php">Agregar Estudio</a></td>
					</p>
					
	     </fieldset>
               </p>
					<p>		 
		  <fieldset>
			       <legend id="pregradot">POSGRADO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los títulos.</label>
			        <table id="tablaPostgrado" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPostgradoTitulos"> 
		   	             <th id="pos1"><label>Modalidad Académica</label></th>
			             <th id="pos2"><label>Titulo que otorga el programa</label></th> 
		   	             <th id="pos3"><label>Semestres Aprobados</label></th> 
			             <th id="pos4"><label>Grad.</label></th> 
						 <th id="pos5"><label>Fecha Grado</label></th> 
						 <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php while ($rs2 = mysql_fetch_assoc($row2)) { ?>
                        <tr>
						<td><label><?php echo $rs2['modalidad']; ?></label></td>
						<td><label><?php echo $rs2['tituloObtenido']; ?></label></td>
						<td><label><?php echo $rs2['semestresAprobados']; ?><label></td>
						<td><label><?php echo $rs2['graduado']; ?><label></td>
						<td><label><?php echo $rs2['fechaGrado']; ?><label></td>
						
						<td><a href="formeditarPostgrado.php?id=<?php echo $rs2['idFormacionAcademica']; ?>">Editar</a></td>
						<td><a href="borrarPostgrado.php?id=<?php echo $rs2['idFormacionAcademica']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
					<p>
					<td><a href="forminsertarFormacionPostgrado.php">Agregar Estudio</a></td>
					</p>
					</p>
			   </fieldset>		
		     </fieldset> 
			  <p>
			    <fieldset id="idiomas">
                 <legend>IDIOMAS</legend>
				 <p>
				 <label id="inf">Por favor ingresar la información sobre sus conocimientos en otros idiomas.</label>
                  </p>				
				<p>
			      <label id="nativo">Idioma Nativo:</label>
				  <input type='text'  value="<?php echo $row4[0]; ?>" name="idioma" disabled=true>
				 </p>
				 <p>
			     <label id="not">Nota: Especifique los Idiomas diferentes al Nativo</label>
                     <table id="tablaIdiomas" border="1" cellspacing="0" cellpadding="0"> 
			         <tr> 
		   	           <th id="idi1"><label>Idioma</label></th> 
			           <th id="idi2"><label>Lo Habla</label></th> 
			           <th id="idi3"><label>Lo Lee</label></th> 
			           <th id="idi4"><label>Lo Escribe</label></th> 
					   <th id="e"><label>Editar</label></th> 
						<th id="e"><label>Borrar</label></th> 
		   	         </tr> 
					 <?php while ($rs3 = mysql_fetch_assoc($row3)) { ?>
                        <tr>
						<td><label><?php echo $rs3['nombre']; ?></label></td>
						<td><label><?php echo $rs3['habla']; ?></label></td>
						<td><label><?php echo $rs3['lee']; ?><label></td>
						<td><label><?php echo $rs3['escribe']; ?><label></td>
						
						<td id="el"><a href="formeditarIdioma.php?id=<?php echo $rs3['idIdioma']; ?>">Editar</a></td>
						<td id="el"><a href="borrarIdioma.php?id=<?php echo $rs3['idIdioma']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
					 
					 
			        </table> 
			        <p>
					<td><a href="forminsertarIdioma.php">Agregar Idioma</a></td>
					</p>
			      </p>
			    </fieldset>
			  </p>
			 <p>
		  <a href="mostrarPagina1.php"><input type="button" value="Anterior" id="nav"></a> 
		 <a href="mostrarPagina3.php"><input type="button" value="Siguiente" id="nav"></a> 
	    </p>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

