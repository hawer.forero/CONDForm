<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  session_destroy();
  mysql_close();
  header("Location: index.php");
}
?>