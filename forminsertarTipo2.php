<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleProductividad.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		<img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarTipo2.php" method="POST">
		     <p>
			 <fieldset id="productividadIntelectual">
				  <legend>PRODUCTIVIDAD INTELECTUAL</legend>
				<fieldset id="tipo2">
				   <legend>2. ARTICULOS PUBLICADOS EN REVISTAS ESPECIALIZADAS HOMOLOGADAS</legend>
				     <input type="hidden" value="tipo2" name="tipo">
					 <p>
						 <label>Titulo del articulo:</label>
						 <input type="text" name="titulo" value='' id="titulos" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
						 <label>Revista:</label>
						 <input type='text' value='' name="revista">
				    </p>
					<p>
						 <label>Categoria:</label>
						 <input type='text' value='' name="categoria">
				    </p>
					<p>
			                <label id="fecha">Fecha Publicación:</label>
			                 <select name="dia">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano">
							   <option>AAAA</option>
							 <?php  for($i=1924;$i<=2014;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						<p>
						 <label>N°Autores:</label>
						 <input type='number' value='' name="autores" min="1" max="100">
				        </p>
						<p>
						 <label>ISSN:</label>
						 <input type='text' value='' name="issn">
				        </p>
						  <p>
						 <label>Folio:</label>
						 <input type='number' value='' name="folio" min="1" max="1000">
				    </p>
					
					 <p>
						 <input type="submit" value="Guardar" id="guardar1">
						 </p>
	     
				 </fieldset> 		
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

