<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  $query = "SELECT idExperienciaProfesional,tipoVinculacion,empresa,cargo,fechaVinculacion,fechaFinalizacion,folio  FROM cd_experienciaprofesional 
  WHERE identificacion = '$us' AND tipo='profesional'";
  $row = mysql_query($query);
 $query2 = "SELECT idExperienciaProfesional,tipoVinculacion,empresa,cargo,fechaVinculacion,fechaFinalizacion,folio  FROM cd_experienciaprofesional 
  WHERE identificacion = '$us' AND tipo='investigativa'";
  $row2 = mysql_query($query2);
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/stylePagina4.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		
        <fieldset id="experienciaProfesional">
                   <legend>EXPERIENCIA PROFESIONAL</legend>
				   <p>
				   <label id="inf">
				   Por favor ingresar en este recuadro (del más actual al más antiguo) la experiencia profesional certificada. 
				   Si en un solo certificado le relacionan varios contratos, usted deberá ingresar cada uno en una fila del recuadro.
				   Es indispensable que ingrese el número de folio que soporta dicha experiencia. 
				   </label>
				   </p>
				
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
			         <table id="tablaExperienciaP" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPregradoTitulos"> 
		   	             <th id="EP1">Tipo Certificación</th>
			             <th id="EP2"><label>Empresa o Institución</label></th> 
		   	             <th id="EP3"><label>Cargo o Función</label></th> 
						  <th id="EP4"><label>Fecha Vinculación</label></th> 
						   <th id="EP5"><label>Fecha Finalización</label></th> 
						  <th id="EP6"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs = mysql_fetch_assoc($row)) { ?>
                        <tr>
						<td id="EP1"><label><?php echo $rs['tipoVinculacion']; ?></label></td>
						<td id="EP2"><label><?php echo $rs['empresa']; ?></label></td>
						<td id="EP3"><label><?php echo $rs['cargo']; ?><label></td>
						<td id="EP4"><label><?php echo $rs['fechaVinculacion']; ?><label></td>
						<td id="EP5"><label><?php echo $rs['fechaFinalizacion']; ?><label></td>
						<td id="EP6"><label><?php echo $rs['folio']; ?><label></td>
						<td id="e"><a href="formeditarExperienciaP.php?id=<?php echo $rs['idExperienciaProfesional']; ?>">Editar</a></td>
						<td id="e"><a href="borrarExperienciaP.php?id=<?php echo $rs['idExperienciaProfesional']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarExperienciaP.php">Agregar Experiencia</a></td>
					</p>
					
	     
               				   
		     </fieldset> 
			 <p>
                   <fieldset id="tipo11">
				   <legend>EXPERIENCIA INVESTIGATIVA EN INSTITUCIONES REGISTRADAS EN INSTITULAC</legend>
				    <p>
				   <label id="inf">
				   Por favor ingresar en este recuadro (del más actual al más antiguo) la experiencia investigativa certificada. 
				   Si en un solo certificado se relacionan varios  contratos, usted deberá señalar cada uno según la fila del recuadro. 
				   Es indispensable que relacione como se le solicita, los folios que soportan dicha experiencia. 
				   </label>
				   </p>
				   <label id="nota">Nota: Ingresar la información  en el orden cronologico de la obtencion de los certificados.</label>
			         <table id="tabla11" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	            <th id="EP1"><label>Tipo Certificación</label></th>
						 <th id="EP2"><label>Empresa o Institución</label></th>
						  <th id="EP3"><label>Cargo o Función</label></th> 
						   <th id="EP4"><label>Fecha Vinculación</label></th> 
						    <th id="EP5"><label>Fecha Finalización</label></th> 
						    <th id="EP6"><label>Folio</label></th> 
							 <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs2 = mysql_fetch_assoc($row2)) { ?>
                        <tr>
						<td id="EP1"><label><?php echo $rs2['tipoVinculacion']; ?></label></td>
						<td id="EP2"><label><?php echo $rs2['empresa']; ?></label></td>
						<td id="EP3"><label><?php echo $rs2['cargo']; ?><label></td>
						<td id="EP4"><label><?php echo $rs2['fechaVinculacion']; ?><label></td>
						<td id="EP5"><label><?php echo $rs2['fechaFinalizacion']; ?><label></td>
						<td id="EP6"><label><?php echo $rs2['folio']; ?><label></td>
						<td id="e"><a href="formeditarExperienciaI.php?id=<?php echo $rs2['idExperienciaProfesional']; ?>">Editar</a></td>
						<td id="e"><a href="borrarExperienciaP.php?id=<?php echo $rs2['idExperienciaProfesional']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarExperienciaI.php">Agregar Registro</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
			  
			 <p>
		  <a href="mostrarPagina3.php"><input type="button" value="Anterior" id="nav"></a> 
		  <a href="mostrarPagina5.php"><input type="button" value="Siguiente" id="nav"></a> 
	    </p>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

