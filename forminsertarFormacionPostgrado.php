<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleInsertarFormacion.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarFormacionPostgrado.php" method="POST">
		     <p>
			<fieldset id="formacionAcademica">
                   <legend>ESTUDIOS EDUCACIÓN SUPERIOR</legend>
				   <br>
				  <fieldset id="postgrados">
			       <legend id="pregradot">POSGRADO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtencion de los titulos.</label>
				   <p>
				   <label>Modalidad Academica:</label>
				   <select name="modalidad">
		            <option>Seleccione Una</option>
			       <option>ESPECIALIZACIÓN</option>
			       <option>MAESTRIA</option>
				   <option>DOCTORADO</option>
	               </select>
				   </p>
				   <p>
				   <label>Titulo que otorga el programa:</label>
				   <input id="tituloO" type='text' value='' name="tituloObtenido"  onChange="javascript:this.value=this.value.toUpperCase();" required>
				   </p>
				   <p>
				   <label>Semestres Aprobados:</label>
				   <input id="nSemestres" type='number' value='' name="semestresAprobados" min="1" max="10" required>
				   </p>
				   <p>
				   <label>Graduado:</label>
				   <select name="graduado">
		            <option>Seleccione Una</option>
			       <option>SI</option>
			       <option>NO</option>
	               </select>
				   </p>
				    <p>
			                <label id="fecha">Fecha de Grado:</label>
			                 <select name="dia">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes" id="mes">
							  <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
			                 <select name="ano">
							   <option>AAAA</option>
						 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
		                 </p>
						 <p>
						 <input type="submit" value="Guardar" id="nav">
						 </p>
				</fieldset>
	            </fieldset>
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

