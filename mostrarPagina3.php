<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  $query = "SELECT idExperienciaDocente,institucion,fechaVinculacion,fechaFinalizacion,folio  FROM cd_experienciadocente 
  WHERE identificacion = '$us' AND `dedicacion`='TC'";
  $row = mysql_query($query);
  $query2 = "SELECT idExperienciaDocente,institucion,fechaVinculacion,fechaFinalizacion,folio  FROM cd_experienciadocente 
  WHERE identificacion = '$us' AND `dedicacion`='MT'";
  $row2 = mysql_query($query2);
  $query3 = "SELECT idExperienciaDocente,institucion,horasSemana,semanasSemestre,horasSemestre,fechaVinculacion,fechaFinalizacion,folio  FROM cd_experienciadocente 
  WHERE identificacion = '$us' AND `dedicacion`='MC'";
  $row3 = mysql_query($query3);
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/stylePagina3.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
       <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		
        <fieldset id="experienciaDocente">
                   <legend>EXPERIENCIA DOCENTE</legend>
				   <p>
				   <label id="inf">
				   En este apartado usted deberá diligenciar cronológicamente (del más actual al más antiguo) la experiencia que posea como docente 
				   de Tiempo Completo, Medio Tiempo y en Modalidad de Cátedra. Si en un solo certificado le relacionan varios  contratos, usted deberá 
				  ingresar cada uno en una fila del recuadro. Es indispensable que relacione como se le solicita, los folios que soportan dicha 
				   experiencia.
				   </label>
				   </p>
				   <p>
				  <fieldset>
			       <legend id="experienciat">TIEMPO COMPLETO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
			        <table id="tablaTC" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPregradoTitulos"> 
		   	             <th id="TC1"><label>Institucion</label></th>
			             <th id="TC2"><label>Fecha Vinculación</label></th> 
		   	             <th id="TC3"><label>Fecha Finalización</label></th> 
						  <th id="TC4"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs = mysql_fetch_assoc($row)) { ?>
                        <tr>
						<td><label><?php echo $rs['institucion']; ?></label></td>
						<td><label><?php echo $rs['fechaVinculacion']; ?></label></td>
						<td><label><?php echo $rs['fechaFinalizacion']; ?><label></td>
						<td><label><?php echo $rs['folio']; ?><label></td>
						<td><a href="formeditarExperienciaTC.php?id=<?php echo $rs['idExperienciaDocente']; ?>">Editar</a></td>
						<td><a href="borrarExperienciaTC.php?id=<?php echo $rs['idExperienciaDocente']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
					<p>
					<td><a href="forminsertarExperienciaTC.php">Agregar Experiencia</a></td>
					</p>
					
	     </fieldset>
               </p>
				 <p>
				  <fieldset>
			       <legend id="experienciat">MEDIO TIEMPO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
			        <table id="tablaTC" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPregradoTitulos"> 
		   	             <th id="TC1"><label>Institucion</label></th>
			             <th id="TC2"><label>Fecha Vinculación</label></th> 
		   	             <th id="TC3"><label>Fecha Finalización</label></th> 
						  <th id="TC4"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs2 = mysql_fetch_assoc($row2)) { ?>
                        <tr>
						<td><label><?php echo $rs2['institucion']; ?></label></td>
						<td><label><?php echo $rs2['fechaVinculacion']; ?></label></td>
						<td><label><?php echo $rs2['fechaFinalizacion']; ?><label></td>
						<td><label><?php echo $rs2['folio']; ?><label></td>
						<td><a href="formeditarExperienciaMT.php?id=<?php echo $rs2['idExperienciaDocente']; ?>">Editar</a></td>
						<td><a href="borrarExperienciaMT.php?id=<?php echo $rs2['idExperienciaDocente']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
					<p>
					<td><a href="forminsertarExperienciaMT.php">Agregar Experiencia</a></td>
					</p>
					
	     </fieldset>
               </p>	
               <p>
				  <fieldset>
			       <legend id="experienciat">MODALIDAD DE CÁTEDRA</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la obtención de los certificados.</label>
			        <table id="tablaTC" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id="tablaPregradoTitulos"> 
		   	             <th id="MC1"><label>Institucion</label></th>
						 <th id="MC2"><label>N°horas/<br>Semana</label></th>
						 <th id="MC3"><label>N°Sem/<br>Semestre</label></th>
						 <th id="MC4"><label>Total horas/<br>Semestre</label></th>
			             <th id="MC5"><label>Fecha<br> Vinculación</label></th> 
		   	             <th id="MC6"><label>Fecha<br>Finalización</label></th> 
						  <th id="MC7"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs3 = mysql_fetch_assoc($row3)) { ?>
                        <tr>
						<td id="MC1"><label><?php echo $rs3['institucion']; ?></label></td>
						<td id="MC2"><label><?php echo $rs3['horasSemana']; ?></label></td>
						<td id="MC3"><label><?php echo $rs3['semanasSemestre']; ?></label></td>
						<td id="MC4"><label><?php echo $rs3['horasSemestre']; ?></label></td>
						<td id="MC5"><label><?php echo $rs3['fechaVinculacion']; ?></label></td>
						<td id="MC6"><label><?php echo $rs3['fechaFinalizacion']; ?><label></td>
						<td id="MC7"><label><?php echo $rs3['folio']; ?><label></td>
						<td id="e"><a href="formeditarExperienciaMC.php?id=<?php echo $rs3['idExperienciaDocente']; ?>">Editar</a></td>
						<td id="e"><a href="borrarExperienciaMC.php?id=<?php echo $rs3['idExperienciaDocente']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
					<p>
					<td><a href="forminsertarExperienciaMC.php">Agregar Experiencia</a></td>
					</p>
					
	     </fieldset>
               </p>					   
		     </fieldset> 
			  
			 <p>
		  <a href="mostrarPagina2.php"><input type="button" value="Anterior" id="nav"></a> 
		   <a href="mostrarPagina4.php"><input type="button" value="Siguiente" id="nav"></a> 
	    </p>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

