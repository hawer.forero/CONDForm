<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/styleFormulario.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
<script src="js/validar.js"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
       
	    
		 <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		<p>
		<label id="into1">
		Señor concursante, antes de iniciar el diligenciamiento del Formato de Hoja de Vida en línea, se le sugiere leer la 
		<a href="#">Resolución Rectoral N° 2957 de 2014.</a>
		</label>
		</p>
		<p>
		<label id="into2">
		Es importante que usted, tenga en cuenta que deberá contar a la mano con su hoja de vida organizada y  debidamente foliada, 
		pues en ciertos apartados se le solicitara información donde tendrá que relacionar el folio correspondiente. Considere, 
		que el Formato de Hoja de Vida requerido por la universidad deberá ser impreso por usted y foliado junto con los soportes 
		correspondientes. 
		</label>
		</p>
		<label id="into3">
		No olvide que una vez consolidada su hoja de vida, deberá entregarla como señala la <a href="#"> Resolución Rectoral N° 2957 de 2014 </a>en 
		el Articulo 2.
		</label>
		</p>
		   <form id="form2" action="insertarDatosPersonales.php" method="POST">
		   <p>
		    <fieldset id="datosPersonales">
		   <legend>USUARIO Y CONTRASEÑA</legend>
               <p>
			   <label id="nota">Nota: Tenga presente su USUARIO (el cual es su N°. Identificación)  y CONTRASEÑA porque 
			   una vez guarde esta página estos datos serán solicitados cuando desee volver a ingresar. </label>
               </p>	
                <p>
			   <label id="nota">El N° Identificación no debe contener puntos</label>
			   </p>			   
			   <p>
			   <label id="tipo">Tipo/N° Identificación:</label>
			   <select name="tipoid">
		       <option>C.C</option>
			   <option>C.E</option>
			   <option>PAS</option>
	           </select>
			   <input type='text' value='' name="identificacion" id="identificacion" required>
			   <label id="cont">Contraseña:</label>
			   <input type="password" name="contrasena2" id="cont2" required>
			    </p>
			
				</fieldset>
				</p>
		     <p>
	         <fieldset id="datosConcurso">
             <legend>DATOS DEL CONCURSO AL CUAL APLICA</legend>
			  <p>	
	            <label>Facultad:</label>
			     <select name="facultad" onchange="ajustar(this,this.form.unidad);" id="facultad">
				  <option>Seleccione una</option>
				 <option value="agropecuarias">FACULTAD DE CIENCIAS AGROPECUARIAS Y RECURSOS NATURALES</option>
				 <option value="ingenierias">FACULTAD DE CIENCIAS BÁSICAS E INGENIERÍA</option>
				 <option value="economicas">FACULTAD DE CIENCIAS ECONÓMICAS</option>
				 <option value="humanas">FACULTAD DE CIENCIAS HUMANAS Y DE LA EDUCACIÓN</option>
				 <option value="salud">FACULTAD DE CIENCIAS DE LA SALUD</option>
				</select>
		      </p>
		      <p>
		         <label>Unidad Académica: <br>(Escuela, Departamento o Instituto)</label>
		         <select name="unidad" onchange="ajustar(this,this.form.area);" id="unidad">
		          <option>Seleccione una</option>
	          </select>
		      </p>
		      <p> 
		         <label>Area:</label>
		         <select name="area" onchange="ajustar(this,this.form.concurso);" id="area">
		         <option>Seleccione una</option>
	             </select>
		      </p>
		      <p>
		       <label>N°.Concurso (Codigo):</label>
		       <select name="concurso" id="concurso">
		       <option>Seleccione una</option>
	          </select> 
		      </p>
          </fieldset>
		  </p>
		  
		  <p>
		   <fieldset id="datosPersonales">
		    <legend>DATOS PERSONALES</legend>	
			 
               <p>
               <label>1er Apellido:</label>
			   <input type='text'  value='' name="apellido1" id="apellido1" onChange="javascript:this.value=this.value.toUpperCase();" required>
			   <label id="apellido2do">2do Apellido:</label>
			   <input type='text' value='' name="apellido2" id="apellido2" onChange="javascript:this.value=this.value.toUpperCase();" required>
		       </p>
		       <p>
			   <label>Nombres:</label>
			   <input type='text' value='' name="nombre" id="nombre"onChange="javascript:this.value=this.value.toUpperCase();" required>
			   <label id="nac">Nacionalidad:</label>
			    <input type='text'  value='' name="nacionalidad" id="nacionalidad" onChange="javascript:this.value=this.value.toUpperCase();" required>
		       </p>
		        <p>
		        <label>Sexo:</label>
			    <input type="radio" name="sexo" value='FEMENINO' id="sexo"><label id="f">F</label>
			    <input type="radio" name="sexo" value='MASCULINO' id="sexo"><label id="m">M</label>
				<label id="tel">Teléfono:</label>
			    <input type='text'  value='' name="telefono" id="telefono">
		        </p>
		        <p>
			    
			    <label id="cel">Celular 1:</label>
			    <input type='text'  value='' name="celular1" id="celular1" required>
				 <label id="cel2">Celular 2:</label>
			    <input type='text'  value='' name="celular2" id="celular2">
		        </p>
		        <p class="correo">
			    <label>Email:</label>
			    <input type='email' value='' name="email" id="email" required>
		       </p>
			    <p class="correo">
			    <label>Idioma Nativo:</label>
			    <input type='text' value='' name="idioma" onChange="javascript:this.value=this.value.toUpperCase();" required>
		       </p>
            </fieldset>
		    </p>
			 <p>
	                    <fieldset id="fechaNacimiento">
	                     <legend>FECHA Y LUGAR DE NACIMIENTO</legend>
	                     <p>
						 <label>Fecha:</label>
			                 <select name="ano">
					         <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
					
			                 <select name="mes" id="mes">
						     <option>MM</option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
							 
							 <select name="dia">
							 <option>DD</option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
							
			                
							
		                 </p>
		                 <article id="lugarNacimiento">
		                    <p>
			                 <label>Pais:</label>
			                 <input type='text'  value='' name="paisNacimiento" id="pais_n" onChange="javascript:this.value=this.value.toUpperCase();" required>
		                    </p>
		                    <p>
			                 <label>Departamento:</label>
			                 <input type='text'  value='' name="depNacimiento" id="departamento_n" onChange="javascript:this.value=this.value.toUpperCase();" required>
		                    </p>
		                    <p>
			                 <label>Municipio:</label>
			                 <input type='text' value='' name="munNacimiento" id="municipio_n" onChange="javascript:this.value=this.value.toUpperCase();" required> 
		                     <br><br>
		                    </p>
		                 </article>
	                    </fieldset>
	                 </p>
	                 <p>
	                    <fieldset id="direccionCorrespondencia">
                         <legend>DIRECCIÓN DE CORRESPONDENCIA</legend>
		                 <p>
			               <label>Pais:</label>
			               <input type='text'  value='' name="paisCorrespondencia"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		                 </p>
		                 <p>
			               <label>Departamento:</label>
			               <input type='text'  value='' name="depCorrespondencia"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		                 </p>
                         <p>		
			                <label>Municipio:</label>
			                <input type='text' value='' name="munCorrespondencia"  onChange="javascript:this.value=this.value.toUpperCase();" required>
		                 </p>
		                 <p>
			                <label>Dirección: </label>
			                <input type='text'  value='' name="direccion" id="direccion" onChange="javascript:this.value=this.value.toUpperCase();" required> 
		                    
		                 </p>
						 <p>
			                <label>Barrio: </label>
			                <input type='text'  value='' name="barrio" id="direccion" onChange="javascript:this.value=this.value.toUpperCase();" required> 
		                    
		                 </p>
			 
 	                    </fieldset>
	                 </p>
		  <input type="submit" value="Registrarse y Continuar" id="guardar1" onClick="return validaciones();">
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html>
