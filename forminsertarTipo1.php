<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleProductividad.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        
	    <section id="login">
		
         <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		<img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarTipo1.php" method="POST">
		     <p>
			 <fieldset id="productividadIntelectual">
				  <legend>PRODUCTIVIDAD INTELECTUAL</legend>
				  <p>
				<fieldset id="tipo1">
				   <legend>1. ARTICULOS REVISTAS INDEXADAS U HOMOLOGADAS (COLCIENCIAS)</legend>
				     <input type="hidden" value="tipo1" name="tipo">
					 <p>
						 <label>Titulo del articulo:</label>
						 <input type="text" name="nombre" value='' id="titulos" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
						 <label>Revista:</label>
						 <input type='text' value='' name="revista" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
						 <label>Categoria:</label>
						 <select name="categoria">
						 <option>Seleccione una</option>
						 <option>A1</option>
						 <option>A2</option>
						 <option>B</option>
						 <option>C</option>
						 </select>
				    </p>
					<p>
						 <label>Volumen:</label>
						 <input type='number' value='' name="volumen">
				    </p>
					<p>
						 <label>Numero:</label>
						 <input type='text' value='' name="numero">
				    </p>
					<p>
					<label>Año:</label>
					 <select name="ano">
							   <option>AAAA</option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
					</p>
					
						<p>
						 <label>N°Autores:</label>
						 <input type='number' value='' name="autores" min="1" max="100" required>
				        </p>
						<p>
						 <label>ISSN:</label>
						 <input type='text' value='' name="issn">
				        </p>
						  <p>
						 <label>Folios del </label>
						 <input type='number' value='' name="folio" min="1" max="1000" required>
						 <label id="al">al</label>
						 <input type='number' value='' name="folio2" min="1" max="1000" required>
						 
				    </p>
					
					 <p>
						 <input type="submit" value="Guardar" id="nav">
						 </p>
	     
				 </fieldset> 
</p>				 
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

