<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Idiomas</title>
<link rel="stylesheet" href="css/styleInsertarFormacion.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="insertarIdioma.php" method="POST">
		     <p>
			  <fieldset id="idiomas">
                 <legend>IDIOMAS</legend>
				  <label id="not">Nota: Especifique los Idiomas diferentes al Nativo</label>
				 <p>
                 <label>Idioma:</label> 
			     <input type="text" name="nombre" onChange="javascript:this.value=this.value.toUpperCase();" required> 
			     </p>
				 <p>
                 <label>Lo Habla:</label> 
			     <select name="habla">
				  <option>Seleccione una</option>
				 <option>REGULAR</option>
				 <option>BIEN</option>
				 <option>MUY BIEN</option>
				 </select>
			     </p>
				  <p>
                 <label>Lo Lee:</label> 
			     <select name="lee">
				 <option>Seleccione una</option>
				 <option>REGULAR</option>
				 <option>BIEN</option>
				 <option>MUY BIEN</option>
				 </select>
			     </p>
				  <p>
                 <label>Lo Escribe:</label> 
			     <select name="escribe">
				  <option>Seleccione una</option>
				 <option>REGULAR</option>
				 <option>BIEN</option>
				 <option>MUY BIEN</option>
				 </select>
			     </p>
			    </fieldset>
	          
		  </p>
		  
		  <input type="submit" value="Guardar" id="nav">
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

