function ajustar(origen,destino)
 {
   function asignar(texto,valor)
   {
     this.texto = texto;
     this.valor = valor;
    }    
         var agropecuarias=new Array()
    	 agropecuarias[0] = new asignar('Seleccione una')
		 agropecuarias[1] = new asignar("INSTITUTO DE ACUICULTURA DE LOS LLANOS",'acuicultura')
		 agropecuarias[2] = new asignar("ESCUELA DE INGENIERÍA EN CIENCIAS AGRARIAS",'agrarias')
    	 agropecuarias[3] = new asignar("ESCUELA DE CIENCIAS ANIMALES",'animales')
    	
    	 
         var ingenierias=new Array()
    	 ingenierias[0] = new asignar('Seleccione una')
    	 ingenierias[1] = new asignar("DEPARTAMENTO DE BIOLOGÍA Y QUÍMICA",'fcbi1')
    	 ingenierias[2] = new asignar("DEPARTAMENTO DE MATEMÁTICAS Y FÍSICA",'fcbi2')
		 ingenierias[3] = new asignar("INSTITUTO DE CIENCIAS AMBIENTALES",'fcbi3') 
		 ingenierias[4] = new asignar("ESCUELA DE INGENIERÍA",'fcbi4') 
		 
         var economicas=new Array()
    	 economicas[0] = new asignar('Seleccione una')
		 economicas[1] = new asignar("ESCUELA DE ECONOMÍA Y FINANZAS",'fce1')	
    	 economicas[2] = new asignar("ESCUELA DE ADMINISTRACIÓN Y NEGOCIOS",'fce2')
		 
         var humanas=new Array()
    	 humanas[0] = new asignar('Seleccione una')
    	 humanas[1] = new asignar("ESCUELA DE PEDAGOGÍA Y BELLAS ARTES",'fche1')	
		 humanas[2] = new asignar("ESCUELA DE HUMANIDADES",'fche2')	
		 
         var salud=new Array()
    	 salud[0] = new asignar('Seleccione una')
    	 salud[1] = new asignar("ESCUELA DE CUIDADO",'fcs1')
		 salud[2] = new asignar("ESCUELA DE SALUD PÚBLICA",'fcs2')
		 //area
		 var acuicultura = new Array()
		 acuicultura[0] = new asignar('Seleccione una')
    	 acuicultura[1] = new asignar("ACUICULTURA Y PESCA",'acuypesca')
    	 acuicultura[2] = new asignar("BIOLOGÍA DE ORGANISMOS ACUÁTICOS (ACUICULTURA)",'acuaticos')
		
		 var agrarias = new Array()
    	 agrarias[0] = new asignar('Seleccione una')
    	 agrarias[1] = new asignar("SUELOS",'suelos')
    	 agrarias[2] = new asignar("MECANIZACIÓN DE SUELOS (AGRICULTURA DE PRECISIÓN)",'mecanizacion')
    	 agrarias[3] = new asignar("SANIDAD VEGETAL (FITOPATOLÓGIA)",'sanidad')
		 agrarias[4] = new asignar("GESTION DE PROCESOS",'gestion')
		 agrarias[5] = new asignar("PROCESOS AGROINDUSTRIALES (OPERACIONES UNITARIAS)",'unitarias')
		  
         var animales = new Array()
    	 animales[0] = new asignar('Seleccione una')
    	 animales[1] = new asignar("ENFERMEDADES INFECCIOSAS",'eca1')
    	 animales[2] = new asignar("HISTOLOGíA ANIMAL",'eca2')
		 animales[3] = new asignar("ADMINISTRACIÓN AGROPECUARIA",'eca3')
		 animales[4] = new asignar("PRODUCCIÓN AVÍCOLA O PORCÍCOLA",'eca4')
		 
		 var fcbi1 = new Array()
		 fcbi1[0] = new asignar('Seleccione una')
    	 fcbi1[1] = new asignar("QUÍMICA GENERAL Y ANALÍTICA",'dbq1')
    	 fcbi1[2] = new asignar("BIOLOGÍA (ECOLOGÍA VEGETAL)",'dbq2')
         fcbi1[3] = new asignar("BIOLOGÍA (ECOLOGÍA ANIMAL)",'dbq3')
		 
		 var fcbi2 = new Array()
		 fcbi2[0] = new asignar('Seleccione una')
    	 fcbi2[1] = new asignar("MATEMATÍCAS",'dmf1')
    	 fcbi2[2] = new asignar("MATEMATÍCAS - ESTADISTICA",'dmf2')
         fcbi2[3] = new asignar("FISICA",'dmf3')
		 
		 var fcbi3 = new Array()
		 fcbi3[0] = new asignar('Seleccione una')
    	 fcbi3[1] = new asignar("ECONOMÍA AMBIENTAL",'ica1')
		 
		 var fcbi4 = new Array()
		 fcbi4[0] = new asignar('Seleccione una')
    	 fcbi4[1] = new asignar("AUTOMATIZACIÓN",'ei1')
    	 fcbi4[2] = new asignar("ELECTRÓNICA DIGITAL",'ei2')
         fcbi4[3] = new asignar("INVESTIGACIÓN DE OPERACIONES Y SIMULACIÓN COMPUTACIONAL",'ei3')
         
		 var fce1 = new Array()
		 fce1[0] = new asignar('Seleccione una')
    	 fce1[1] = new asignar("CONTABLE",'eef1')
    	 fce1[2] = new asignar("ECONOMÍA",'eef2')
         fce1[3] = new asignar("PROYECTOS",'eef3')
		 fce1[4] = new asignar("FINANZAS",'eef4')
		 
		 var fce2 = new Array()
		 fce2[0] = new asignar('Seleccione una')
    	 fce2[1] = new asignar("ADMINISTRACIÓN Y ORGANIZACIONES",'ean1')
    	 fce2[2] = new asignar("MERCADEO",'ean2')
         fce2[3] = new asignar("MERCADEO INTERNACIONAL",'ean3')
        
         var fche1 = new Array()
		 fche1[0] = new asignar('Seleccione una')
    	 fche1[1] = new asignar("EDUCACIÓN",'epb1')
		 fche1[2] = new asignar("EDUCACIÓN INFANTIL",'epb2')
		 fche1[3] = new asignar("DIDÁCTICA DE LAS CIENCIAS AGROPECUARIAS",'epb3')
		 fche1[4] = new asignar("DIDÁCTICA DE LA FISICA",'epb5')
		 fche1[5] = new asignar("DIDÁCTICA DE LAS MATEMATICAS",'epb6')
		 fche1[6] = new asignar("EDUCACIÓN FISICA",'epb7')
		 
		  var fche2 = new Array()
		 fche2[0] = new asignar('Seleccione una')
    	 fche2[1] = new asignar("SOCIOLOGÍA (SOCIEDAD Y CULTURA)",'eh1')
		 fche2[2] = new asignar("FILOSOFÍA (EPISTEMOLOGÍA)",'eh2')
		 
         var fcs1 = new Array()
		 fcs1[0] = new asignar('Seleccione una')
    	 fcs1[1] = new asignar("FARMACIA",'ec1')
		 fcs1[2] = new asignar("REGENCIA DE FARMACIA",'ec2')
		 fcs1[3] = new asignar("CUIDADO CLÍNICO DEL ADULTO",'ec3')
		 fcs1[4] = new asignar("CUIDADO CLÍNICO DEL INFANTE",'ec4')
		 fcs1[5] = new asignar("CUIDADO MATERNO-INFANTIL",'ec5')
		 
		 var fcs2 = new Array()
		 fcs2[0] = new asignar('Seleccione una')
    	 fcs2[1] = new asignar("SALUD MENTAL",'esp1')
		 fcs2[2] = new asignar("ADMINISTRACIÓN EN SALUD",'esp2')
		 fcs2[3] = new asignar("SALUD OCUPACIONAL",'esp3')
		 fcs2[4] = new asignar("SALUD FAMILIAR O LA ATENCION PRIMARIA EN SALUD",'esp4')
		 fcs2[5] = new asignar("SALUD PÚBLICA",'esp5')
		 
    
		 
    	 //INSTITUTO DE ACUICULTURA DE LOS LLANOS
		 var acuypesca = new Array()
    	 acuypesca[0] = new asignar("01-IALL-02-14",null)
         var acuaticos = new Array()
    	 acuaticos[0] = new asignar("02-IALL-02-14",null)
		 
		 //ESCUELA Ingenieria ciencias agrarias 
		 var suelos = new Array()
    	 suelos[0] = new asignar("01-EICA-02-14",null)
		 var mecanizacion = new Array()
    	 mecanizacion[0] = new asignar("02-EICA-02-14",null)
		 var sanidad = new Array()
    	 sanidad[0] = new asignar("03-EICA-02-14",null)
		 var gestion = new Array()
    	 gestion[0] = new asignar("04-EICA-02-14",null)
		 var unitarias = new Array()
    	 unitarias[0] = new asignar("05-EICA-02-14",null)
		 //escuela ciencias animales
		  var eca1 = new Array()
    	 eca1[0] = new asignar("01-ECA-02-14",null)
          var eca2 = new Array()
    	 eca2[0] = new asignar("02-ECA-02-14",null)
		  var eca3 = new Array()
    	 eca3[0] = new asignar("03-ECA-02-14",null)
		  var eca4 = new Array()
    	 eca4[0] = new asignar("04-ECA-02-14",null)
		 
		 var dbq1 = new Array()
    	 dbq1[0] = new asignar("01-DBQ-02-14",null)
		 var dbq2 = new Array()
    	 dbq2[0] = new asignar("02-DBQ-02-14",null)
		 var dbq3 = new Array()
    	 dbq3[0] = new asignar("03-DBQ-02-14",null)
          
		 var dmf1 = new Array()
    	 dmf1[0] = new asignar("01-DMF-02-14",null)
		 var dmf2 = new Array()
    	 dmf2[0] = new asignar("02-DMF-02-14",null)
		 var dmf3 = new Array()
    	 dmf3[0] = new asignar("03-DMF-02-14",null)
		 
		 var ica1 = new Array()
    	 ica1[0] = new asignar("01-ICA-02-14",null)
		 
		 var ei1 = new Array()
    	 ei1[0] = new asignar("01-EI-02-14",null)
		 var ei2 = new Array()
    	 ei2[0] = new asignar("02-EI-02-14",null)
		 var ei3 = new Array()
    	 ei3[0] = new asignar("03-EI-02-14",null)
    	 
		 var eef1 = new Array()
    	 eef1[0] = new asignar("01-EEF-02-14",null)
		 var eef2 = new Array()
    	 eef2[0] = new asignar("02-EEF-02-14",null)
		 var eef3 = new Array()
    	 eef3[0] = new asignar("03-EEF-02-14",null)
		 var eef4 = new Array()
    	 eef4[0] = new asignar("04-EEF-02-14",null)
		 
		 var ean1 = new Array()
    	 ean1[0] = new asignar("01-EAN-02-14",null)
		  var ean2 = new Array()
    	 ean2[0] = new asignar("02-EAN-02-14",null)
		  var ean3 = new Array()
    	 ean3[0] = new asignar("03-EAN-02-14",null)
		 
		 var epb1 = new Array()
    	 epb1[0] = new asignar("01-EPB-02-14",null)
		 var epb2 = new Array()
    	 epb2[0] = new asignar("02-EPB-02-14",null)
		
		var epb3 = new Array()
    	 epb3[0] = new asignar("03-EPB-02-14",null)
    	 epb3[1] = new asignar("04-EPB-02-14",null)
		
		var epb5 = new Array()
    	 epb5[0] = new asignar("05-EPB-02-14",null)
		 var epb6 = new Array()
    	 epb6[0] = new asignar("06-EPB-02-14",null)
		 var epb7 = new Array()
    	 epb7[0] = new asignar("07-EPB-02-14",null)
		 
		 var eh1 = new Array()
    	 eh1[0] = new asignar("01-EH-02-14",null)
		 var eh2 = new Array()
    	 eh2[0] = new asignar("02-EH-02-14",null)
		 
		  var ec1 = new Array()
    	 ec1[0] = new asignar("01-EC-02-14",null)
		  var ec2 = new Array()
    	 ec2[0] = new asignar("02-EC-02-14",null)
		  var ec3 = new Array()
    	 ec3[0] = new asignar("03-EC-02-14",null)
		  var ec4 = new Array()
    	 ec4[0] = new asignar("04-EC-02-14",null)
		  var ec5 = new Array()
    	 ec5[0] = new asignar("05-EC-02-14",null)
		 
		 var esp1 = new Array()
    	 esp1[0] = new asignar("01-ESP-02-14",null)
		 var esp2 = new Array()
    	 esp2[0] = new asignar("02-ESP-02-14",null)
		 var esp3 = new Array()
    	 esp3[0] = new asignar("03-ESP-02-14",null)
		 var esp4 = new Array()
    	 esp4[0] = new asignar("04-ESP-02-14",null)
		  var esp5 = new Array()
    	 esp5[0] = new asignar("05-ESP-02-14",null)
		  esp5[1] = new asignar("06-ESP-02-14",null)
		 
	  if(origen.selectedIndex != 0)
	   {
		destino.length=0
		origen = eval(origen.value)
		 for(m=0;m<origen.length;m++)
		{
			var nuevaOpción = new Option(origen[m].texto);
			destino.options[m] = nuevaOpción;
			if(origen[m].valor != null)
			{
				destino.options[m].value = origen[m].valor
			}
			else
			{
				destino.options[m].value = origen[m].texto
			}
		}
	  }
     }