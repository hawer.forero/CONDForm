<!doctype html>
<?php
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
  $us = $_SESSION['usuario'];
  require('seguridad.php');
  conectar_base_datos();
  $query = "SELECT idProductividadIntelectual,nombre,revista,categoria,volumen,numero,ano,autores,issn,folio,folio2  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo1'";
  $row = mysql_query($query);
 
  
  $query2 = "SELECT idProductividadIntelectual,nombre,isbn,autores,fechaTerminacion,editorial  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo2'";
  $row2 = mysql_query($query2);
  
  $query3 = "SELECT idProductividadIntelectual,nombre,isbn,autores,fechaTerminacion,editorial  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo3'";
  $row3 = mysql_query($query3);
  
  $query4 = "SELECT idProductividadIntelectual,nombre,autores,fechaTerminacion,editorial,nombreTraduccion,autoresTraduccion,fechaTraduccion,editorialTraduccion,folioAutorizacion  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo4'";
  $row4 = mysql_query($query4);
  
   $query5 = "SELECT idProductividadIntelectual,nombre,entidadFinanciadora,fechaInicio,fechaTerminacion,folio FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo5'";
  $row5 = mysql_query($query5);
  
   $query6 = "SELECT idProductividadIntelectual,nombre,institucion,fechaTerminacion,folio FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo6'";
  $row6 = mysql_query($query6);
  
  $query7 = "SELECT idProductividadIntelectual,nombre,institucion,fechaTerminacion,folio FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo7'";
  $row7 = mysql_query($query7);
  
  
   $query8 = "SELECT idProductividadIntelectual,nombre,revista,fechaTerminacion,autores,folio  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo8'";
  $row8 = mysql_query($query8);
  
  $query9 = "SELECT idProductividadIntelectual,nombre,numeroRegistro,fechaTerminacion,entidadOtorga,folio  FROM cd_productividadintelectual
  WHERE identificacion = '$us' AND tipo = 'tipo9'";
  $row9 = mysql_query($query9);
  
  
 
 ?>
<html>
<head>
<meta charset="utf-8"/>
<title>Formulario de Inscripción</title>
<link rel="stylesheet" href="css/stylePagina5.css"/>
<script src="js/ajustar-select.js" type="text/javascript"></script>
</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo " <section id='salir'> Usuario: <b>$us</b> ";  echo" <a href='salir.php'> Cerrar Sesión </a></section>";?>
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		
        <fieldset id="productividadIntelectual">
                   <legend>PRODUCTIVIDAD INTELECTUAL</legend>
				   <p>
				   <label id="inf">
				   En este recuadro usted podrá relacionar cronológicamente (del más actual al más antiguo) para todos. 
				   </label>
				   </p>
				   <p>
				<fieldset id="tipo1">
				   <legend>1. ARTICULOS REVISTAS INDEXADAS U HOMOLOGADAS (COLCIENCIAS)</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico a la publicación.</label>
			         <table id="tabla1" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="a1"><label>Título del articulo</label></th>
						 <th id="a2"><label>Revista</label></th>
						 <th id="a3"><label>Cat</label></th>
						 <th id="a4"><label>Vol.</label></th> 
						 <th id="a5"><label>Num.</label></th>
						 <th id="a6"><label>Año</label></th> 						   
						 <th id="a7"><label>Aut.</label></th> 
						 <th id="a8"><label>ISSN</label></th> 
						 <th id="a9"><label>Folios</label></th> 
						  <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs = mysql_fetch_assoc($row)) { ?>
                        <tr>
						<td id="a1"><label><?php echo $rs['nombre']; ?></label></td>
						<td id="a2"><label><?php echo $rs['revista']; ?></label></td>
						<td id="a3"><label><?php echo $rs['categoria']; ?><label></td>
						<td id="a4"><label><?php echo $rs['volumen']; ?><label></td>
						<td id="a5"><label><?php echo $rs['numero']; ?><label></td>
						<td id="a6"><label><?php echo $rs['ano']; ?><label></td>
						<td id="a7"><label><?php echo $rs['autores']; ?><label></td>
						<td id="a8"><label><?php echo $rs['issn']; ?><label></td>
						<td id="a9"><label><?php echo $rs['folio'].' al '.$rs['folio2']; ?><label></td>
						<td id="e"><a href="formeditarTipo1.php?id=<?php echo $rs['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo1.php?id=<?php echo $rs['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo1.php">Agregar Articulo Revista Indexada</a></td>
					</p>
					
	        
               		</fieldset> 
					</p>
					<p>
                   <fieldset id="tipo3">
				   <legend>2. LIBROS PRODUCTO DE INVESTIGACIÓN</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la publicación.</label>
			         <table id="tabla3" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="b1"><label>Título del Libro</label></th>
						 <th id="b2"><label>ISBN</label></th>
						 <th id="b3"><label>Autores</label></th> 
						 <th id="b4"><label>Fecha Publicación</label></th> 
						 <th id="b5"><label>Editorial</label></th>
						  <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs2 = mysql_fetch_assoc($row2)) { ?>
                        <tr>
						<td id="b1"><label><?php echo $rs2['nombre']; ?></label></td>
						<td id="b2"><label><?php echo $rs2['isbn']; ?></label></td>
						<td id="b3"><label><?php echo $rs2['autores']; ?><label></td>
						<td id="b4"><label><?php echo $rs2['fechaTerminacion']; ?><label></td>
						<td id="b5"><label><?php echo $rs2['editorial']; ?><label></td>
						<td id="e"><a href="formeditarTipo3.php?id=<?php echo $rs2['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs2['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo3.php">Agregar Libro producto Investigación</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
						<p>
                   <fieldset id="tipo4">
				   <legend>3. LIBROS DE TEXTO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronologico a la publicación.</label>
			         <table id="tabla4" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="b1"><label>Título del Libro</label></th>
						 <th id="b2"><label>ISBN</label></th>
						 <th id="b3"><label>Autores</label></th> 
						 <th id="b4"><label>Fecha Publicación</label></th> 
						 <th id="b5"><label>Editorial</label></th>
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs3 = mysql_fetch_assoc($row3)) { ?>
                        <tr>
						<td id="b1"><label><?php echo $rs3['nombre']; ?></label></td>
						<td id="b2"><label><?php echo $rs3['isbn']; ?></label></td>
						<td id="b3"><label><?php echo $rs3['autores']; ?><label></td>
						<td id="b4"><label><?php echo $rs3['fechaTerminacion']; ?><label></td>
						<td id="b5"><label><?php echo $rs3['editorial']; ?><label></td>
						<td id="e"><a href="formeditarTipo4.php?id=<?php echo $rs3['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs3['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo4.php">Agregar Libro de Texto</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo5">
				   <legend>4. TRADUCCIONES DE LIBRO</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico a la publicación.</label>
			         <table id="tabla5" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="c1"><label>Titulo del Libro(Original)</label></th>
						 <th id="c2"><label>Aut</label></th> 
						 <th id="c3"><label>Fecha Publicación</label></th> 
						 <th id="c4"><label>Editorial</label></th>
						 <th id="c5"><label>Titulo del Libro (Traducción)</label></th>
						 <th id="c6"><label>Aut</label></th> 
						 <th id="c7"><label>Fecha Publicación</label></th> 
						 <th id="c8"><label>Editorial</label></th>
						 <th id="c9"><label>Folio Autori</label></th>
                           <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 						  
		   	            </tr> 
						<?php  while ($rs4 = mysql_fetch_assoc($row4)) { ?>
                        <tr>
						<td id="c1"><label><?php echo $rs4['nombre']; ?></label></td>
						<td id="c2"><label><?php echo $rs4['autores']; ?><label></td>
						<td id="c3"><label><?php echo $rs4['fechaTerminacion']; ?><label></td>
						<td id="c4"><label><?php echo $rs4['editorial']; ?><label></td>
						<td id="c5"><label><?php echo $rs4['nombreTraduccion']; ?></label></td>
						<td id="c6"><label><?php echo $rs4['autoresTraduccion']; ?><label></td>
						<td id="c7"><label><?php echo $rs4['fechaTraduccion']; ?><label></td>
						<td id="c8"><label><?php echo $rs4['editorialTraduccion']; ?><label></td>
						<td id="c9"><label><?php echo $rs4['folioAutorizacion']; ?><label></td>
						<td id="e"><a href="formeditarTipo5.php?id=<?php echo $rs4['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo5.php?id=<?php echo $rs4['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo5.php">Agregar Traduccion de libro</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo6">
				   <legend>5. PROYECTOS DE INVESTIGACION</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico a la Terminación.</label>
			         <table id="tabla6" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="b1"><label>Nombre del Proyecto</label></th>
						 <th id="b2"><label>Entidad Financiadora</label></th> 
						 <th id="b3"><label>Fecha Inicio</label></th> 
						 <th id="b4"><label>Fecha Terminación</label></th> 
						  <th id="b5"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs5 = mysql_fetch_assoc($row5)) { ?>
                        <tr>
						<td id="b1"><label><?php echo $rs5['nombre']; ?></label></td>
						<td id="b2"><label><?php echo $rs5['entidadFinanciadora']; ?></label></td>
						<td id="b3"><label><?php echo $rs5['fechaInicio']; ?><label></td>
						<td id="b4"><label><?php echo $rs5['fechaTerminacion']; ?><label></td>
						<td id="b5"><label><?php echo $rs5['folio']; ?><label></td>
						<td id="e"><a href="formeditarTipo6.php?id=<?php echo $rs5['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs5['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo6.php">Agregar Proyecto de Investigacion</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo7">
				   <legend>6. DOCUMENTOS DE CONDICIONES MINIMAS PRESENTADOS ANTE EL MEN</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronológico de la terminación.</label>
			         <table id="tabla7" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="d1"><label>Nombre del Documento</label></th>
						 <th id="d2"><label>Institución</label></th> 
						 <th id="d3"><label>Fecha Terminación</label></th> 
						  <th id="d4"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs6 = mysql_fetch_assoc($row6)) { ?>
                        <tr>
						<td id="d1"><label><?php echo $rs6['nombre']; ?></label></td>
						<td id="d2"><label><?php echo $rs6['institucion']; ?></label></td>
						<td id="d3"><label><?php echo $rs6['fechaTerminacion']; ?><label></td>
						<td id="d4"><label><?php echo $rs6['folio']; ?><label></td>
						<td id="e"><a href="formeditarTipo7.php?id=<?php echo $rs6['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs6['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo7.php">Agregar Documento</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo8">
				   <legend>7. DOCUMENTOS INSTITUCIONALES DE ACREDITACION DE CALIDAD</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronologico de la terminación.</label>
			         <table id="tabla8" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	             <th id="d1"><label>Nombre del Documento</label></th>
						 <th id="d2"><label>Institucion</label></th> 
						 <th id="d3"><label>Fecha Terminacion</label></th> 
						  <th id="d4"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs7 = mysql_fetch_assoc($row7)) { ?>
                        <tr>
						<td id="d1"><label><?php echo $rs7['nombre']; ?></label></td>
						<td id="d2"><label><?php echo $rs7['institucion']; ?></label></td>
						<td id="d3"><label><?php echo $rs7['fechaTerminacion']; ?><label></td>
						<td id="d4"><label><?php echo $rs7['folio']; ?><label></td>
						<td id="e"><a href="formeditarTipo8.php?id=<?php echo $rs7['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs7['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo8.php">Agregar Documento</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo9">
				   <legend>8. ARTICULOS EN REVISTAS ACADEMICAS NO INDEXADAS</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronologico de la publicación.</label>
			         <table id="tabla9" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	            <th id="b1"><label>Titulo del articulo</label></th>
						 <th id="b2"><label>Revista</label></th>
						  <th id="b3"><label>Fecha Publicación</label></th> 
						   <th id="b4"><label>Autores</label></th> 
						  <th id="b5"><label>Folio</label></th> 
						   <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 
		   	            </tr> 
						<?php  while ($rs8 = mysql_fetch_assoc($row8)) { ?>
                        <tr>
						<td id="b1"><label><?php echo $rs8['nombre']; ?></label></td>
						<td id="b2"><label><?php echo $rs8['revista']; ?></label></td>
						<td id="b3"><label><?php echo $rs8['fechaTerminacion']; ?><label></td>
						<td id="b4"><label><?php echo $rs8['autores']; ?><label></td>
						<td id="b5"><label><?php echo $rs8['folio']; ?><label></td>
						<td id="e"><a href="formeditarTipo9.php?id=<?php echo $rs8['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs8['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo9.php">Agregar Articulo</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
                   <fieldset id="tipo10">
				   <legend>9. REGISTROS ANTE LA DIRECCION NACIONAL DE DERECHOS DE AUTOR, DE PATENTES O DE VARIEDADES</legend>
				   <label id="nota">Nota: Ingresar la información  en el orden cronologico de la obtencion de los titulos.</label>
			         <table id="tabla10" border="1" cellspacing="0" cellpadding="0"> 
			            <tr id=""> 
		   	            <th id="b1"><label>Nombre del Registro</label></th>
						 <th id="b2"><label>Numero</label></th>
						  <th id="b3"><label>Fecha</label></th> 
						   <th id="b4"><label>Entidad que Otorga</label></th> 
						  <th id="b5"><label>Folio</label></th>
                         <th id="e"><label>Editar</label></th> 
						 <th id="e"><label>Borrar</label></th> 						  
		   	            </tr> 
						<?php  while ($rs9 = mysql_fetch_assoc($row9)) { ?>
                        <tr>
						<td id="b1"><label><?php echo $rs9['nombre']; ?></label></td>
						<td id="b2"><label><?php echo $rs9['numeroRegistro']; ?></label></td>
						<td id="b3"><label><?php echo $rs9['fechaTerminacion']; ?><label></td>
						<td id="b4"><label><?php echo $rs9['entidadOtorga']; ?><label></td>
						<td id="b5"><label><?php echo $rs9['folio']; ?><label></td>
						<td id="e"><a href="formeditarTipo10.php?id=<?php echo $rs9['idProductividadIntelectual']; ?>">Editar</a></td>
						<td id="e"><a href="borrarTipo4.php?id=<?php echo $rs9['idProductividadIntelectual']; ?>">Borrar</a></td>
						
						</tr>
                        <?php } ?>
			        </table> 
			        
					<p>
					<td><a href="forminsertarTipo10.php">Agregar Registro</a></td>
					</p>
					
	        
               		</fieldset> 
                    </p>
					<p>
					 <label id="inf">
				   Apreciado concursante, la Universidad de los Llanos agradece su participación en la presente convocatoria.
				   <br>
                   Recuerde:
				   <ul>
				   <li>Imprimir el archivo PDF generado al FINALIZAR este formulario de inscripción, anexándole los soportes establecidos en la <a href="#">Resolución Rectoral N° 2957 de 2014,</a> debidamente clasificados y foliados.
				   </li>	
                  <li> Imprimir el <a href="formatosobre.html" target="_blank">Formato de Presentación</a> para marcar el sobre de manila para el envio de todos los documentos mencionados. </li>			   
				   </label>
					
					<P>
		     </fieldset> 
			  
			 <p>
		  <a href="mostrarPagina4.php"><input type="button" value="Anterior" id="nav"></a> 
		  <a href="Imprimir.php" target="_blank"><input type="button" value="Finalizar" id="nav"></a> 
	    
	  </p>
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

