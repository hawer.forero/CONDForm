<?php
require('seguridad.php');
session_start();
if(!isset($_SESSION['usuario']))
{
  header("Location: index.php");
}
else
{
$us = $_SESSION['usuario'];
$idExperienciaProfesional = $_GET["id"];
conectar_base_datos();
 
  $query = mysql_query("SELECT tipoVinculacion,empresa,cargo,DATE_FORMAT(fechaVinculacion,'%e'),DATE_FORMAT(fechaVinculacion,'%m'),DATE_FORMAT(fechaVinculacion,'%Y'),DATE_FORMAT(fechaFinalizacion,'%e'),DATE_FORMAT(fechaFinalizacion,'%m'),DATE_FORMAT(fechaFinalizacion,'%Y'),folio FROM `cd_experienciaprofesional` WHERE `identificacion`='$us'AND `idExperienciaProfesional`=$idExperienciaProfesional");
  $row = mysql_fetch_array($query);
  if($row[4]==01){$mes='Enero';}
  if($row[4]==02){$mes='Febrero';}
  if($row[4]==03){$mes='Marzo';}
  if($row[4]==04){$mes='Abril';}
  if($row[4]==05){$mes='Mayo';}
  if($row[4]==06){$mes='Junio';}
  if($row[4]==07){$mes='Julio';}
  if($row[4]==8){$mes='Agosto';}
  if($row[4]==9){$mes='Septiembre';}
  if($row[4]==10){$mes='Octubre';}
  if($row[4]==11){$mes='Noviembre';}
  if($row[4]==12){$mes='Diciembre';}
   if($row[7]==01){$mes2='Enero';}
  if($row[7]==02){$mes2='Febrero';}
  if($row[7]==03){$mes2='Marzo';}
  if($row[7]==04){$mes2='Abril';}
  if($row[7]==05){$mes2='Mayo';}
  if($row[7]==06){$mes2='Junio';}
  if($row[7]==07){$mes2='Julio';}
  if($row[7]==8){$mes2='Agosto';}
  if($row[7]==9){$mes2='Septiembre';}
  if($row[7]==10){$mes2='Octubre';}
  if($row[7]==11){$mes2='Noviembre';}
  if($row[7]==12){$mes2='Diciembre';}
  

?>
<html>
<head>
<meta charset="utf-8"/>
<title>Insertar Formacion Academica</title>
<link rel="stylesheet" href="css/styleInsertarExperienciaD.css"/>

</head>
<body>
    <article id="contenido1">
	  <section id="encabezado">
        <img src="img/corocora.png"/>   
        <h2>UNIVERSIADAD DE LOS LLANOS<br>VICERRECTORÍA ACADÉMICA</h2>
	    <section id="login">
		
        <?php echo "Usuario: <b>$us</b> ";  echo"<a href='salir.php'>Salir</a>";?>	
		
		</section>
		
		</section>
		<section id="titulo">
        <h1>FORMATO DE HOJA DE VIDA PARA PARTICIPAR<br>CONCURSO DE MÉRITOS PROFESORES DE PLANTA 02-P-2014</h1>
        </section>
		   <form action="editarExperienciaP.php" method="POST">
		    <input name="idExperienciaProfesional" type="hidden" value="<?php echo"$idExperienciaProfesional";?>" > 
		     <p>
			 <fieldset id="experienciaProfesional">
                   <legend>EXPERIENCIA PROFESIONAL</legend>

					 <p>
						 <label>Tipo de Certificacion:</label>
						 <select name="tipoVinculacion">
						 <option><?php echo"$row[0]";?></option>
						   <option value="C.LAB">C.LAB - CERTIFICACION LABORAL</option>
						 <option value="D.JUR">D.JUR - DECLARACION JURAMENTADA</option>
						 <option value="C.CPS">C.CPS - CERTIFICADO DE CUMPLIMIENTO DE CONTRATO DE PRESTACIÓN DE SERVICIOS</option>
						 
						 </select>
				    </p>
					<p>
						 <label>Empresa:</label>
						 <input type='text' value='<?php echo"$row[1]";?>' name="empresa" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
						 <label>Cargo:</label>
						 <input type='text' value='<?php echo"$row[2]";?>' name="cargo" onChange="javascript:this.value=this.value.toUpperCase();" required>
				    </p>
					<p>
			                <label id="fecha">Fecha Vinculacion:</label>
							  <select name="ano">
							   <option><?php echo"$row[5]";?></option>
							  <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                
			                 <select name="mes" id="mes">
							  <option value='<?php echo"$row[4]";?>'><?php echo"$mes";?></option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
							  <select name="dia">
							 <option><?php echo"$row[3]";?></option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			               
		                 </p>
						 <p>
			                <label id="fecha">Fecha Finalizacion:</label>
							 <select name="ano2">
							   <option><?php echo"$row[8]";?></option>
							 <?php  for($i=2014;$i>=1924;$i--){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                 <select name="mes2" id="mes">
							  <option value='<?php echo"$row[7]";?>'><?php echo"$mes2";?></option>
							 <option value="01">Enero</option>
							 <option value="02">Febrero</option>
							 <option value="03">Marzo</option>
							 <option value="04">Abril</option>
						     <option value="05">Mayo</option>
						     <option value="06">Junio</option>
						     <option value="07">Julio</option>
						     <option value="08">Agosto</option>
							 <option value="09">Septiembre</option>
							 <option value="10">Octubre</option>
							 <option value="11">Noviembre</option>
							 <option value="12">Diciembre</option>
		                     </select>
							 <select name="dia2">
							 <option><?php echo"$row[6]";?></option>
							 <?php  for($i=1;$i<=31;$i++){?>
							 <option><?php echo"$i";?></option><?php } ?>
		                     </select>
			                
		                 </p>
						  <p>
						 <label>Folio:</label>
						 <input type='number' value='<?php echo"$row[9]";?>' name="folio" min="1" max="1000" required>
				    </p>
					 <p>
						 <input type="submit" value="Guardar" id="nav">
						 </p>
	     
						
		     </fieldset> 
						 
						
				
	          
		  </p>
		  
		  
	     </form>
	  
	  
	  
	  
	</article>




</body>
</html> 
 
 
 
 <?php
}

 ?>

